#### Слайды лекции
 * [тык](https://docs.google.com/presentation/d/1eDaQ68qZJ1x2DeFFyFM-zVrUfj6g-eQvS4Rey7Z_Yt8/edit?usp=sharing)
 
#### Слайды семинара (в нем же домашка)
 * [тык](https://docs.google.com/presentation/d/1DQsbaqiykG1aZ_L90rHKloYlql7DwXKKrzJPWgq5wxc/edit?usp=sharing)
 * [табличка для мерж реквестов](https://docs.google.com/spreadsheets/d/1k-DnzR7S68UhizjYZBNrWlL_SDymNNIgxZleJkzcRkA/edit?usp=sharing)

#### Полезные материалы:
 * [скачать git](https://git-scm.com)
 * [визуализатор](https://git-school.github.io/visualizing-git/)
 * [документация git'а](https://git-scm.com/docs)
 * [обучение гиту в игровой форме, очень рекомендую](https://learngitbranching.js.org/?locale=ru_RU)
 