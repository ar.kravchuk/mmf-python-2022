#### Слайды лекции
 * [тык](https://docs.google.com/presentation/d/1f7sh9eeRWQHR9CDi6vhBYFYGMXPkDiJNJpH9x1ksB-g/edit?usp=sharing)

#### Слайды семинаров
 * [тык](https://docs.google.com/presentation/d/1yScaGCQRdyRblst9N_damOXOZg1ieyjE3kEaIv8Mh3U/edit#slide=id.g104a6f1eb5d_0_100)
 * [табличка для мерж реквестов](https://docs.google.com/spreadsheets/d/1k-DnzR7S68UhizjYZBNrWlL_SDymNNIgxZleJkzcRkA/edit?usp=sharing)

#### Полезные материалы:
 * [Документация python](https://docs.python.org/3/)
 * [Хорошие ютуб лекции по python](https://www.youtube.com/watch?v=-py9GXvJk6A&list=PLlb7e2G7aSpQhNphPSpcO4daaRPeVstku&ab_channel=ComputerScienceCenter)
   