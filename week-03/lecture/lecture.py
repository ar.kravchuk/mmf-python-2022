import logging
from functools import wraps

logging.basicConfig(filename='log1.log', level=logging.DEBUG)
DEBUG = True


def debug(res, func_name, *args, **kwargs):
    if DEBUG:
        logging.debug(f'{args}, {kwargs}, {res}, {func_name}')


def debug_decorator(func):
    @wraps(func)
    def inner(*args, **kwargs):
        res = func(*args, **kwargs)
        debug(res, func.__name__, *args, **kwargs)
        return res
    return inner


@debug_decorator
def my_max(*elements):
    res = 0
    for elem in elements:
        if elem > res:
            res = elem
    return res


@debug_decorator
def my_sum(*elements):
    res = 0
    for elem in elements:
        res -= elem
    return res


def max_of_sums(*lists):
    sums = []

    for list_ in lists:
        sums.append(my_sum(*list_))
    return my_max(*sums)


if __name__ == '__main__':
    # callback = choose_callback(False)
    print(my_max.__name__)

    # print(max_of_sums([1, 2], [5, 6, 6], [100], [100, 1]))
