from functools import wraps


def mock(return_value):
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            return return_value

        return wrapper

    return decorator


@mock(return_value='haha')
def f(x):
    return x


if __name__ == '__main__':
    print(f(1))
